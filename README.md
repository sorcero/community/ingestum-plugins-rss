# ingestum-plugins-rss

## Plugins directory
The directory of the plugins must be specified in an environment variable as follows.
```
export INGESTUM_PLUGINS_DIR="/path_to_your_plugins/ingestum-plugins-rss/plugins/"
```

## Documentation
For more information about Ingestum plugins, see the [documentation](https://sorcero.gitlab.io/community/ingestum/plugins.html).

## Note
Some plugins have additional dependencies. Please refer to the README.md files in the individual plugin subdirectories.
